--Cliente (DNI, nombre, apellido, dirección, tarifa)
DROP TABLE IF EXISTS Cliente;
CREATE TABLE IF NOT EXISTS Cliente(
  DNI INT CHECK(DNI > 0) NOT NULL,
  nombre VARCHAR(255) NOT NULL,
  apellido VARCHAR(255) NOT NULL,
  direccion VARCHAR(255) NOT NULL,
  tarifa INT CHECK(tarifa BETWEEN 0 AND 10000),
  PRIMARY KEY (DNI)
);

--Categoría (nro_categoria, tasa)
DROP TABLE IF EXISTS Categoria;
CREATE TABLE IF NOT EXISTS Categoria(
  nro_categoria INT NOT NULL,
  tasa INT,
  PRIMARY KEY (nro_categoria)
);

CREATE TYPE marc AS ENUM ('FIAT', 'RENAULT', 'FORD');
--Automovil (patente, marca, modelo, dni, nro_categoria)
DROP TABLE IF EXISTS Automovil;
CREATE TABLE IF NOT EXISTS Automovil(
  patente VARCHAR(10) NOT NULL,
  marca marc NOT NULL,
  modelo INT CHECK(modelo BETWEEN 1990 and 2015) NOT NULL,
  DNI INT REFERENCES Cliente(DNI) ON DELETE SET NULL,
  nro_categoria INT REFERENCES Categoria(nro_categoria),
  PRIMARY KEY (patente)
);

--Taller (nro_taller, nombre, dirección)
DROP TABLE IF EXISTS Taller;
CREATE TABLE IF NOT EXISTS Taller(
  nro_taller SERIAL,
  nombre VARCHAR,
  dirección VARCHAR,
  PRIMARY KEY (nro_taller)
);

--Accidente (nro_accidente, DNI, patente, nro_taller, fecha, costo)
DROP TABLE IF EXISTS Accidente;
CREATE TABLE IF NOT EXISTS Accidente(
  nro_accidente SERIAL,
  DNI INT REFERENCES Cliente(DNI) ON DELETE CASCADE,
  patente VARCHAR(10) NOT NULL REFERENCES Automovil(patente),
  nro_taller INT REFERENCES Taller(nro_taller),
  fecha DATE,
  costo INT,
  PRIMARY KEY (nro_accidente)
);