--CREATE DATABASE IF NOT EXISTS ejer1a;

--Cliente (nro_cliente, apellido, nombre, dirección, teléfono)
DROP TABLE IF EXISTS Cliente;
CREATE TABLE IF NOT EXISTS Cliente(
    nro_cliente SERIAL,
    apellido VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    telefono INT NOT NULL,
    PRIMARY KEY (nro_cliente)
);

--Producto (cod_producto, descripción, precio, stock_minimo, stock_maximo, cantidad)
CREATE TABLE IF NOT EXISTS Producto(
  cod_producto SERIAL,
  descripción VARCHAR(255),
  stock_minimo INT NOT NULL CHECK(stock_minimo >0),
  stock_maximo INT NOT NULL CHECK(stock_maximo >0),
  PRIMARY KEY (cod_producto)
);

--Factura (nro_factura, nro_cliente, fecha, monto)
DROP TABLE IF EXISTS Factura;
CREATE TABLE IF NOT EXISTS Factura (
  nro_factura SERIAL,
  nro_cliente INT NOT NULL,
  fecha DATE,
  monto INT NOT NULL,
  FOREIGN KEY (nro_cliente) REFERENCES Cliente(nro_cliente),
  PRIMARY KEY (nro_factura)
);

--ItemFactura (cod_producto, nro_factura, cantidad, precio)
DROP TABLE IF EXISTS ItemFactura;
CREATE TABLE IF NOT EXISTS ItemFactura(
  cod_producto INT NOT NULL,
  nro_factura INT NOT NULL,
  cantidad INT NOT NULL CHECK(cantidad > 0),
  precio INT NOT NULL CHECK(precio > 0),
  PRIMARY KEY (nro_factura, cod_producto),
  FOREIGN KEY (cod_producto) REFERENCES Producto(cod_producto),
  FOREIGN KEY (nro_factura) REFERENCES Factura(nro_factura)
);
