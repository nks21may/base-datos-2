--Vehiculo (#patente, marca, modelo, color, saldoActual)
CREATE TABLE Vehiculo(
  nro_patente INT NOT NULL,
  marca VARCHAR(255) NOT NULL,
  modelo VARCHAR(255) NOT NULL,
  color VARCHAR(255) NOT NULL,
  saldoActual INT NOT NULL,
  PRIMARY KEY (nro_patente)
);

--Persona (dni, nombreYApellido, direccion)
CREATE TABLE Persona(
  dni INT NOT NULL,
  nombreYApellido VARCHAR(255),
  direccion VARCHAR(255),
  PRIMARY KEY (dni)
);

--Dueño (#patente, dni) 
CREATE TABLE Dueno(
  id INT NOT NULL,
  nro_patente INT NOT NULL,
  dni INT NOT NULL,
  FOREIGN KEY (dni) REFERENCES Persona(dni),
  FOREIGN KEY (nro_patente) REFERENCES Vehiculo(nro_patente)
  PRIMARY KEY (id)
);

--Parquímetro (#parquímetro, calle, altura)
CREATE TABLE Parquimetro(
  nro_parquimetro INT not NULL,
  calle VARCHAR(255),
  altura VARCHAR(255),
  PRIMARY KEY (nro_parquimetro)
);

--Estacionamiento   (#estacionamiento,   #patente,   #parquímetro,   fecha,   saldoInicio,saldoFinal, horaEntrada, horaSalida)
CREATE TABLE Estacionamiento(
  nro_estacionamiento INT NOT NULL,
  nro_patente INT NOT NULL,
  nro_parquimetro INT,
  fecha DATE,
  saldoInicio INT,
  saldoFinal INT,
  horaEntrada TIMESTAMP,
  horaSalida TIMESTAMP,
  FOREIGN KEY (nro_patente) REFERENCES Vehiculo(nro_patente),
  FOREIGN KEY (nro_parquimetro) REFERENCES Parquimetro(nro_parquimetro),
  PRIMARY KEY (nro_estacionamiento)
);