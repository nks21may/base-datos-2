CREATE DATABASE IF NOT EXISTS ejer1b;

#Vehiculo (#patente, marca, modelo, color, saldoActual)
CREATE TABLE IF NOT EXISTS ejer1b.Vehiculo(
  nro_patente INT NOT NULL,
  marca VARCHAR(255) NOT NULL,
  modelo VARCHAR(255) NOT NULL,
  color VARCHAR(255) NOT NULL,
  saldoActual INT NOT NULL,
  PRIMARY KEY (nro_patente)
)ENGINE=InnoDB;

#Persona (dni, nombreYApellido, direccion)
CREATE TABLE IF NOT EXISTS ejer1b.Persona(
  dni INT NOT NULL,
  nombreYApellido VARCHAR(255),
  direccion VARCHAR(255),
  PRIMARY KEY (dni)
) ENGINE = InnoDB;

#Dueño (#patente, dni)
CREATE TABLE IF NOT EXISTS ejer1b.Dueno(
  id INT NOT NULL,
  nro_patente INT NOT NULL,
  dni INT NOT NULL,
  FOREIGN KEY (dni) REFERENCES Persona(dni),
  FOREIGN KEY (nro_patente) REFERENCES Vehiculo(nro_patente),
  PRIMARY KEY (id)
) ENGINE = InnoDB;

#Parquímetro (#parquímetro, calle, altura)
CREATE TABLE IF NOT EXISTS ejer1b.Parquimetro(
  nro_parquimetro INT not NULL,
  calle VARCHAR(255),
  altura VARCHAR(255),
  PRIMARY KEY (nro_parquimetro)
) ENGINE = InnoDB;

#Estacionamiento   (#estacionamiento,   #patente,   #parquímetro,   fecha,   saldoInicio,saldoFinal, horaEntrada, horaSalida)
CREATE TABLE IF NOT EXISTS ejer1b.Estacionamiento(
  nro_estacionamiento INT NOT NULL,
  nro_patente INT NOT NULL,
  nro_parquimetro INT,
  fecha DATE,
  saldoInicio INT,
  saldoFinal INT,
  horaEntrada TIME,
  horaSalida TIME,
  FOREIGN KEY (nro_patente) REFERENCES Vehiculo(nro_patente),
  FOREIGN KEY (nro_parquimetro) REFERENCES Parquimetro(nro_parquimetro),
  PRIMARY KEY (nro_estacionamiento)
) ENGINE = InnoDB;
