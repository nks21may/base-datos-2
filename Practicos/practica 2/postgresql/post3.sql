--cree los usuarios “asesor”, “administrativo” y “encargado”, teniendo en cuenta:
CREATE ROLE empleado;
GRANT SELECT(nombre, apellido) ON Cliente TO empleado;
--El rol de empleado puede consultar los campos apellido y nombre de la tabla cliente.
CREATE USER asesor IN ROLE empleado;
CREATE USER administrativo IN ROLE empleado;
CREATE USER encargado IN ROLE empleado;
--a) Todos los usuarios pertenecen al rol “empleado”.


--b) El “asesor”, ademas de lo que le rol empleado le permite, puede consultar la tabla taller e insertar en la tabla accidente.
GRANT SELECT ON Taller TO asesor;
GRANT INSERT ON Accidente TO asesor;

--c) El “administrativo” puede consultar la tabla automóvil.
GRANT SELECT ON Automovil TO administrativo;

--d) El usuario “encargado” tiene los privilegios de borrar todas las tablas y actualizar el campo tasa de la tabla categoría.
GRANT DELETE ON ALL TABLES IN SCHEMA public TO encargado;
GRANT UPDATE(tasa) ON Categoria TO encargado;
