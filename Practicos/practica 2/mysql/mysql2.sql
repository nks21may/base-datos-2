#c) Compruebe la correcta definición de permisos (de accesos al servidor y a lasbase de datos).

DROP USER IF EXISTS 'encargado_estacionamiento'@'localhost';
CREATE USER 'encargado_estacionamiento'@'localhost' IDENTIFIED BY 'encargado_estacionamiento';
#b) El usuario “encargado_estacionamiento” tiene los privilegios de consultar todas las tablas de la base de datos y de insertar en la tabla parquímetro en dicha base de datos.
GRANT SELECT, INSERT ON *.* TO 'encargado_estacionamiento'@'localhost';

DROP USER IF EXISTS 'cobrador'@'*';
CREATE USER 'cobrador'@'*' IDENTIFIED BY 'cobrador';
#a) El “cobrador” sólo puede consultar la tabla estacionamiento y realizar 3 conexiones/hora y 10 consultas/hora.
GRANT SELECT ON ejer1b.Estacionamiento TO 'cobrador'@'*' WITH MAX_CONNECTIONS_PER_HOUR 3 MAX_QUERIES_PER_HOUR 10;
#d) Borre el privilegio de consulta del usuario “cobrador”.
REVOKE SELECT ON ejer1b.Estacionamiento FROM 'cobrador'@'*';
