DROP USER IF EXISTS 'vendedor1';
CREATE USER 'vendedor1' IDENTIFIED BY 'vendedor1';
GRANT INSERT ON ejer1a.Cliente TO 'vendedor1';

DROP USER IF EXISTS 'vendedor2';
CREATE USER 'vendedor2' IDENTIFIED BY 'vendedor2';
GRANT DELETE ON ejer1a.Factura TO 'vendedor2';
GRANT SELECT ON ejer1a.Producto TO 'vendedor2';

DROP USER IF EXISTS 'administrador';
CREATE USER 'administrador' IDENTIFIED BY 'administrador';
GRANT UPDATE (descripción) ON ejer1a.Producto TO 'administrador' WITH GRANT OPTION;

GRANT DELETE ON ejer1a.Cliente TO 'vendedor1','vendedor2','administrador';

REVOKE ALL ON ejer1a.Factura FROM 'vendedor2';
REVOKE ALL ON ejer1a.Producto FROM 'vendedor2';