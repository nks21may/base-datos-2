--Cliente (DNI, nombre, apellido, dirección, tarifa)
CREATE TABLE Cliente(
  DNI INT CHECK(DNI > 0) NOT NULL,
  nombre VARCHAR(255) NOT NULL,
  apellido VARCHAR(255) NOT NULL,
  direccion VARCHAR(255) NOT NULL,
  tarifa INT CHECK(tarifa BETWEEN 0 AND 10000),
  PRIMARY KEY (DNI)
);

--Categoría (nro_categoria, tasa)
CREATE TABLE Categoria(
  nro_categoria INT NOT NULL,
  tasa INT,
  PRIMARY KEY (nro_categoria)
);

--Automovil (patente, marca, modelo, dni, nro_categoria)
CREATE TABLE Automovil(
  patente VARCHAR(10) NOT NULL,
  marca VARCHAR(10)  CHECK( marca IN ('FIAT', 'RENAULT', 'FORD')) NOT NULL,
  modelo INT CHECK(modelo BETWEEN 1990 and 2015) NOT NULL,
  DNI INT REFERENCES Cliente(DNI) ON DELETE SET NULL,
  nro_categoria INT REFERENCES Categoria(nro_categoria),
  PRIMARY KEY (patente)
);

--Taller (nro_taller, nombre, dirección)
CREATE TABLE Taller(
  nro_taller INT NOT NULL,
  nombre VARCHAR(255),
  dirección VARCHAR(255),
  PRIMARY KEY (nro_taller)
);

--Accidente (nro_accidente, DNI, patente, nro_taller, fecha, costo)
CREATE TABLE Accidente(
  nro_accidente INT NOT NULL,
  DNI INT REFERENCES Cliente(DNI) ON DELETE CASCADE,
  patente VARCHAR(10) NOT NULL REFERENCES Automovil(patente),
  nro_taller INT REFERENCES Taller(nro_taller),
  fecha DATE,
  costo INT,
  PRIMARY KEY (nro_accidente)
);