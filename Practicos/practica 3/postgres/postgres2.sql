--Banco (cod_banco, nombre, precio, cotizacion_maxima)
CREATE TABLE IF NOT EXISTS Banco (
  cod_banco INT NOT NULL,
  nombre VARCHAR(255),
  precio INT,
  cotizacion_maxima INT NOT null,
  PRIMARY KEY (cod_banco)
);

--Cotización (cod_cotizacion, cod_banco, cotización)
--Cod_banco: clave foránea a Banco
CREATE TABLE IF NOT EXISTS Cotizacion (
  cod_cotizacion INT NOT NULL,
  cod_banco INT REFERENCES Banco(cod_banco),
  cotizacion INT,
  PRIMARY KEY (cod_cotizacion)
);

CREATE OR REPLACE FUNCTION cotizacion_max(banco int) RETURNS INT AS
$$
DECLARE
	registro RECORD;
	max INT;
BEGIN
	max := 0;
	FOR registro IN (select cotizacion from Cotizacion where cod_banco = banco) LOOP
		IF(max < registro.cotizacion) THEN
			max = registro.cotizacion;
		END IF;
     END LOOP;
END;
$$ LANGUAGE plpgsql;
