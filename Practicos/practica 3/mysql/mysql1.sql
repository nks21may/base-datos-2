delimiter $$
CREATE TRIGGER `fiat_uno` BEFORE INSERT ON ejer1a.Factura FOR EACH ROW
BEGIN
    IF (select SUM(cantidad) from ejer1a.ItemFactura where nro_factura = NEW.nro_factura) > NEW.max THEN
    	signal sqlstate '45000';
    END IF;
END; $$
delimiter ;
