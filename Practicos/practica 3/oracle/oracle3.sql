CREATE TABLE IF NOT EXIST cuenta(
nro_cuenta INT,
saldo INT,
PRIMARY KEY (nro_cuenta)
);

CREATE TABLE movimiento(
nro_movimiento INT,
nro_cuenta INT REFERENCES cuenta(nro_cuenta), 
fecha DATE, 
debe INT, 
haber INT,
PRIMARY KEY (nro_movimiento)
);

CREATE OR REPLACE PROCEDURE consitnte(nro IN integer, idebe IN integer, ihaber IN integer) AS
BEGIN
  UPDATE cuenta SET saldo = saldo - idebe + ihaber WHERE (nro_cuenta = nro);
  DBMS_OUTPUT.PUT_LINE('Saldo ' );
 END;
/

CREATE OR REPLACE PROCEDURE summe(nro IN integer, fe IN date, res OUT integer) AS
  CURSOR cMov IS SELECT * FROM movimiento WHERE (nro = nro_cuenta and fecha <= fe);
  regMov cMov%ROWTYPE;
BEGIN
  OPEN cMov;
  res := 0;
  FETCH cMov INTO regMov;
  WHILE cMov%FOUND LOOP
    res := res + regMov.haber - regMov.debe;
    FETCH cMov INTO regMov;
  END LOOP;
  CLOSE cMov;
END;
/
