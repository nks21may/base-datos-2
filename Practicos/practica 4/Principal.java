package bd;

import java.sql.*;
import java.util.Arrays;

import org.apache.commons.cli.*;

public class Principal {
	/*
	 * Teniendo en cuenta el ejercicio 1 a) de la práctica de repaso, cree un
	 * programa java que implemente un Alta y listado de clientes. El programa puede
	 * ser hecho sin Interfaz gráfica, sólo en línea de comandos. Nota: Resolver
	 * utilizando los motores que se utilizaron en la creación de las tablas .
	 */

	static String urlmysql = "jdbc:mysql://localhost/ejer1a";
	static String urlpostgresql = "jdbc:postgresql://localhost:5432";
	static String username = "root";
	static String password = "root";
	static Connection d;

	public static void mysqlAltaClientes(String apellido, String nombre, String direccion, Integer telefono)
			throws SQLException {
		Statement s = d.createStatement();
		String query = "INSERT INTO `Cliente` (nro_cliente, apellido, nombre, direccion, telefono) values (0," + "\""
				+ apellido + "\"" + "," + "\"" + nombre + "\"" + "," + "\"" + direccion + "\"" + "," + "\""
				+ telefono.toString() + "\"" + ");";
		s.execute(query);
	}

	public static void mysqlConsultarClientes() throws SQLException {
		// Class.forName(driver);
		Statement s = d.createStatement();
		ResultSet r = s.executeQuery("SELECT * FROM Cliente;");
		while (r.next()) {
			System.out.print(" DNI: " + r.getString("nro_cliente"));
			System.out.print("; Nombre: " + r.getString("nombre"));
			System.out.print("; Apellido: " + r.getString("apellido"));
			System.out.print("; Email: " + r.getString("direccion"));
			System.out.print("; Telefono: " + r.getInt("telefono"));
			System.out.print("\n");
			System.out.print("\n");
		}
	}
	
	private int ejecutarEsaFuncion(int banco){
		int result = 0;
        try{
        	CallableStatement properCase = d.prepareCall("{ ? = call cotizacion_max( ? ) }");
            properCase.registerOutParameter(1, Types.INTEGER);
            properCase.setInt(2, banco);
            properCase.execute();
            result = properCase.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return result;
	}

	public static void main(String[] args) {

		Options options = new Options();
		options.addOption("m", "mysql", false, "Use mysql.")
				.addOption("p", "postgresql", false, "Use postgresql.")
				.addOption("c", false, "Consultar clientes")
				.addOption("a", false, "Agregar cliente")
				.addOption("n", false, "Nombre")
				.addOption("s", false, "Apellido")
				.addOption("t", false, "Telefono")
				.addOption("d", false, "Direccion");

		final CommandLineParser cmdLineParser = new DefaultParser();
		CommandLine c = null;
		try {
			c = cmdLineParser.parse(options, args);
		} catch (ParseException parseException) {
			System.out.println("ERROR: Unable to parse command-line arguments " + Arrays.toString(args) + " due to: "
					+ parseException);
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("RAAAAAAAAA", options);
		}
		
		if (c.hasOption('m')) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				d = DriverManager.getConnection(urlmysql, username, password);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		if (c.hasOption('p')) {
			try {
				Class.forName("org.postgresql.Driver"); 
				d = DriverManager.getConnection(urlpostgresql, username, password);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		if(c.hasOption('c')) {
			try {
				mysqlConsultarClientes();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if(c.hasOption('a')) {
			try {
				mysqlAltaClientes(c.getOptionValue("n"), c.getOptionValue("s"), c.getOptionValue("d"), Integer.parseInt(c.getOptionValue("t")));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}
